FactoryBot.define do
  
  factory :user do
    transient do
      password {"12345"}
      active {true}
    end
    name {"Joe"}
    sequence(:email) { |n| "user#{n}@example.com" }
    uuid {UniqueIdGenerator.uuid}
    password_hash {User.encrypt_password(password)}
    auth_token {UniqueIdGenerator.hex}
    token_expiry {DateTime.now + 1.day}
    is_active {active}
  end

  factory :administrator do
    association :user, factory: :user, strategy: :create, email: 'admin1@example.com'
    user_id { user.id } # Assign user_id from the associated user
    is_active { true }
    role {Administrator.roles[:moderator]}
    created_by { user.id } # Assign created_by with the associated user's id
  end

  factory :loan do
    transient do
      loan_status {Loan.statuses[:pending]}
    end
    association :user, factory: :user, strategy: :create
    user_id { user.id }
    uuid {UniqueIdGenerator.uuid}
    amount {1000.00}
    term {1}
    status {loan_status}
    start_date {Date.today}
    recovery_date {Date.today + 7}

    trait :paid do
      status { Loan.statuses[:paid] }
    end

    factory :paid_loan, traits: [:paid]
  end

  factory :installment do
    association :loan, factory: :loan, strategy: :create
    uuid {UniqueIdGenerator.uuid}
    loan_id {loan.id}
    expected_amount {loan.amount}
    expected_pay_date  {loan.recovery_date}
    status  {Installment.statuses[:pending]}

    trait :paid do
      status { Installment.statuses[:paid] }
      actual_pay_date {Date.today}
      actual_amount {expected_amount}
    end

    factory :paid_installment, traits: [:paid]

  end

  factory :payment_detail do
    association :installment, factory: :paid_installment, strategy: :create
    installment_id {installment.id}
    payment_ref_id {UniqueIdGenerator.hex}
    payment_gateway {'paypal'}
    status {PaymentDetail.statuses[:successfull]}
    amount {installment.expected_amount}
  end

end