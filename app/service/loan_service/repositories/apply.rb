module LoanService
  module Repositories
    class Apply

      PAYMENT_FREQ = 7

      def self.persist(entity: {})
        rec = map_entity_to_rec(entity)
        rec.save!
        rec.uuid
      end

      private

      def self.map_entity_to_rec(entity)
        Loan.new({
          user_id: entity.user_id,
          amount: entity.amount,
          term: entity.term,
          status: entity.status,
          start_date: entity.application_date,
          recovery_date: get_recovery_date(entity),
          uuid: UniqueIdGenerator.uuid
        })
      end

      def self.get_recovery_date(entity)
        entity.application_date + (PAYMENT_FREQ*entity.term) # weekly payment schedule
      end

    end
  end
end
