require 'rails_helper'
require 'dry/types'
RSpec.describe LoanService::UseCases::InstallmentPayment do
  let!(:active_user) { create(:user) }
  let(:apply_attributes) { { amount: 5000, term: 2 } }
  let!(:approved_loan) do
    apply_attributes = { amount: 5000, term: 2 }
    entity = LoanService::Entities::Helpers::Apply.get_entity(
      apply_attributes, active_user.id
    )
    LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    loan = Loan.last
    loan.status = Loan.statuses[:approved]
    loan.save!
    Loan.find(loan.id)
  end
  let(:valid_attributes) { { amount: (approved_loan.amount * 0.8), loan_uuid: approved_loan.uuid } }
  let(:payment_ref_id) { UniqueIdGenerator.hex }
    
  describe '#run' do
    it 'records successfull payment for approved loan with valid attributes' do
      payment_ref_id = UniqueIdGenerator.hex
      entity = LoanService::Entities::Helpers::InstallmentPayment.get_entity(
        valid_attributes, payment_ref_id
      )
      
      paid_installments = Loan.find(approved_loan.id).installments.paid
      expect(paid_installments.length).to eq(0) # no installmemts paid

      result = described_class.new(entity: entity).run
      expect(result.status).to eq(ResponseEntity::SUCCESS)
      paid_installments = Loan.find(approved_loan.id).installments.paid
      expect(paid_installments.length).to eq(1) # first installmemt marked paid in DB
    end

    it 'returns error for amount less than installment amount' do
      attributes = { amount: 1.00, loan_uuid: approved_loan.uuid }
  
      entity = LoanService::Entities::Helpers::InstallmentPayment.get_entity(
        attributes, payment_ref_id
      )
      result = described_class.new(entity: entity).run
      expect(result.status).to eq(ResponseEntity::ERROR)
    end

    it 'returns error for amount higher than total loan amount' do
      attributes = { amount: approved_loan.amount + 100, loan_uuid: approved_loan.uuid }
  
      entity = LoanService::Entities::Helpers::InstallmentPayment.get_entity(
        attributes, payment_ref_id
      )
      result = described_class.new(entity: entity).run
      expect(result.status).to eq(ResponseEntity::ERROR)
    end

  end
end
