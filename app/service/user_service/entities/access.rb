module UserService
  module Entities
    class Access < Dry::Struct

      attribute :email, Types::Params::String.constrained(format: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i)
      attribute :password_hash, Types::Strict::String
      
    end
  end
end
