class ResponseEntity < Dry::Struct
  SUCCESS = 'success'
  ERROR = 'error'
  STATUSES = Types::String.enum(SUCCESS, ERROR)

  attribute :status, STATUSES
  attribute :data, Types::JSON::Hash
end