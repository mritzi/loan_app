Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :widgets
  root "welcome#index"
  
  namespace :user, defaults: {format: :json} do
    post :login
    post :register
    put :recover
    put :logout
    put :change_password
  end

  namespace :loan, defaults: {format: :json} do
    post :apply
    post :pay_installment
    get :list
    get :detail
  end

  namespace :admin, defaults: {format: :json} do
    namespace :loan do
      get :list
      put :approve
      put :reject
    end
    #additional sub-functions to manage admins based on roles
  end

end
