module LoanService
  module UseCases
    class LoanSummary < Dry::Struct
      attribute :loan_uuid, Types::Strict::String

      def run
        loan_entity = Repositories::LoanSummary.get_entity(
          loan_uuid: loan_uuid
        )
        response_entity(loan_entity)
      end

      private

      def response_entity(loan_entity)
        ResponseEntity.new(
          status: ResponseEntity::SUCCESS,
          data: JSON.parse(loan_entity.to_json)
        )
      end

    end
  end
end
