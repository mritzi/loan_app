require "rails_helper"
RSpec.describe Loan, :type => :model do

  before(:all) do
    @loan1 = create(:loan)
  end
  
  it "is valid with valid attributes" do
    expect(@loan1).to be_valid
    db_loan = Loan.find(@loan1.id)
    expect(db_loan.status).to eq('pending')
  end
end