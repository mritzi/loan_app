require "rails_helper"
RSpec.describe Installment, :type => :model do
  before(:all) do
    @installment1 = create(:installment)
  end
  
  it "is valid with valid attributes" do
    expect(@installment1).to be_valid
    db_installment = Installment.find(@installment1.id)
    db_loan = db_installment.loan
    expect(db_installment.expected_amount).to eq(db_loan.amount)
    expect(db_installment.expected_pay_date).to eq(db_loan.recovery_date)
    expect(db_installment.status).to eq('pending')
    expect(db_installment.actual_amount).to eq(nil)
    expect(db_installment.actual_pay_date).to eq(nil)
    expect(db_loan.term).to eq(1)
    expect(db_loan.status).to eq('pending')
  end
end