module UserService
  module Repositories
    class Register 

      def self.find_user(email: )
        return nil if email.blank?
        User.find_by(email: email).try(:email)
      end

      def self.persist(entity: {})
        rec = map_entity_to_rec(entity)
        rec.set_auth_params
        rec.save!

        rec.auth_token
      end

      private

      def self.map_entity_to_rec(entity)
        User.new({
          email: entity.email,
          uuid: UniqueIdGenerator.uuid,
          password_hash: entity.password_hash,
          is_active: true 
        })
      end

    end
  end
end
