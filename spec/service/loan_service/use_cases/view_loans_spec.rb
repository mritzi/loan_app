require 'rails_helper'
require 'dry/types'
RSpec.describe LoanService::UseCases::ViewLoans do
  let!(:active_user) { create(:user) }
  let!(:approved_loan) do
    apply_attributes = { amount: 5000, term: 2 }
    entity = LoanService::Entities::Helpers::Apply.get_entity(
      apply_attributes, active_user.id
    )
    LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    loan = Loan.last
    loan.status = Loan.statuses[:approved]
    loan.save!
    Loan.find(loan.id)
  end
  let!(:pending_loan) { create(:loan, loan_status: Loan.statuses[:pending])} # created for diff user
  
  describe '#run' do

    it 'returns info of all loans only for relevant user' do
      response_entity = described_class.new(user_id: active_user.id).run
      expect(response_entity.status).to eq(ResponseEntity::SUCCESS)
      expect(response_entity.data[:loans].length).to eq(1)
      expect(response_entity.data[:loans].first["ref_id"]).to eq(approved_loan.uuid)
      expect(response_entity.data[:loans].first["ref_id"]).not_to eq(pending_loan.uuid)
    end
  end

end