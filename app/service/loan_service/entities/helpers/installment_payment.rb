module LoanService
  module Entities
    module Helpers
      class InstallmentPayment

        def self.get_entity(params, payment_ref_id)
          loan = Repositories::LoanInfo.find_by(uuid: params[:loan_uuid])
          LoanService::Entities::InstallmentPayment.new(
            loan: loan,
            amount: params[:amount],
            payment_ref_id: payment_ref_id,
            pending_installments: loan.pending_installments,
            total_outstanding: get_total_outstanding(loan.pending_installments) 
          )
        end

        private 

        def self.get_total_outstanding(pending_installments)
          total_outstanding = 0
          pending_installments.each do |inst|
            total_outstanding += inst.expected_amount
          end
          total_outstanding
        end

      end
    end
  end
end
