module LoanService
  module Repositories
    class ViewLoans
      
      def self.get_entities(user_id: )
        loans = Loan.where(user_id: user_id).order(id: :desc) #skipping pagination for now
        return prepare_entities(loans)
      end

      private
      
      def self.prepare_entities(loans)
        loans.map do |loan|
          Entities::ViewLoan.new(
            ref_id: loan.uuid,
            start_date: loan.start_date,
            end_date: loan.recovery_date,
            amount: loan.amount,
            term: loan.term,
            status: loan.status
          )
        end
      end

    end
  end
end
