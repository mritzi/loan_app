require 'rails_helper'
require 'dry/types'
RSpec.describe LoanService::UseCases::Apply do
  let!(:active_user) { create(:user) }
    
  describe '#run' do
    it 'creates new loan for active user' do
      term = 2
      entity = LoanService::Entities::Apply.new(
        application_date: Date.today() + 7*term,
        user_id: active_user.id,
        amount: 5000.00,
        term: term
      )
      loans = Loan.where(user_id: active_user.id)
      expect(loans.length).to eq(0) # no existing loans for user in DB

      result = described_class.new(entity: entity).run
      expect(result.status).to eq(ResponseEntity::SUCCESS)
      loans = Loan.where(user_id: active_user.id)
      expect(loans.length).to eq(1) # new loan created for user in DB
      expect(loans.first.status).to eq('pending')
    end
  end
end
