module LoanService
  module UseCases
    class Approval < Dry::Struct
      attribute :loan_uuid, Types::Strict::String

      def run
        Repositories::Approval.run(loan_uuid: loan_uuid)
        #user can also be notified & subsequent steps done post approval
      end

    end
  end
end
