module LoanService
  module Repositories
    class LoanInfo
      
      def self.find_by(uuid: )
        Loan.find_by(uuid: uuid)
      end

      def self.recompute_status(payment_entity: )
        loan = find_by(uuid: payment_entity.loan.uuid)
        return nil if loan.pending_installments.length > 0

        loan.status = Loan.statuses[:paid]
        loan.save!
      end

    end
  end
end