module LoanService
  module Entities
    class LoanSummary < Dry::Struct
      
      attribute :loan, ViewLoan
      attribute :installments, Types::Array.of(ViewInstallment)

    end
  end
end
