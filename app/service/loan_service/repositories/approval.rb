module LoanService
  module Repositories
    class Approval

      def self.run(loan_uuid: )
        loan = Loan.find_by(uuid: loan_uuid, status: Loan.statuses[:pending])
        return response_entity(nil) if loan.blank?
        loan.status = Loan.statuses[:approved]
        loan.save!
        response_entity(loan.id)
      end

      private 

      def self.response_entity(loan_id)
        status = loan_id.present? ? ResponseEntity::SUCCESS : ResponseEntity::ERROR
        ResponseEntity.new(
          status: status,
          data: {message: "Request processed for loan approval"}
        )
      end

    end
  end
end
