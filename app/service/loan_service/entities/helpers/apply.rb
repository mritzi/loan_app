module LoanService
  module Entities
    module Helpers
      class Apply

        def self.get_entity(params, user_id)
          LoanService::Entities::Apply.new(
            application_date: Date.today,
            user_id: user_id,
            amount: params[:amount],
            term: params[:term]
          )
        end

      end
    end
  end
end
