require "rails_helper"
RSpec.describe PaymentDetail, :type => :model do
  before(:all) do
    @payment1 = create(:payment_detail)
  end
  
  it "is valid with valid attributes" do
    expect(@payment1).to be_valid
    db_payment = PaymentDetail.find(@payment1.id)
    db_installment = db_payment.installment
    expect(db_payment.amount).to eq(db_installment.actual_amount)
    expect(db_payment.created_at.to_date).to eq(db_installment.actual_pay_date)
    expect(db_installment.status).to eq('paid')
  end
end