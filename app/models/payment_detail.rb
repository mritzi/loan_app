class PaymentDetail < ApplicationRecord
  belongs_to :installment, class_name: Installment.name, primary_key: :id, foreign_key: :installment_id
  
  enum status: {
    pending: 0,
    successfull: 1,
    failed: 2,
    refunded: 3
    # can add more statuses
  }

end
