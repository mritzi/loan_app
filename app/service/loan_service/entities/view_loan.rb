module LoanService
  module Entities
    class ViewLoan < Dry::Struct
      
      attribute :ref_id, Types::Strict::String
      attribute :start_date, Types::Params::Date
      attribute :end_date, Types::Params::Date
      attribute :amount, Types::Params::Decimal
      attribute :term, Types::Strict::Integer
      attribute :status, Types::Strict::String

    end
  end
end
