class Installment < ApplicationRecord
  belongs_to :loan, class_name: Loan.name, primary_key: :id, foreign_key: :loan_id
  has_one :payment, class_name: PaymentDetail.name, primary_key: :id, foreign_key: :installment_id
  #(for future implementation including failed payment records) 
  # has_many :payment_details, class_name: PaymentDetail.name, primary_key: :id, foreign_key: :installment_id

  enum status: {
    pending: 0,
    paid: 1
    # can add more statuses
  }
end