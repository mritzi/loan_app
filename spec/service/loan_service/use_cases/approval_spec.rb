require 'rails_helper'
require 'dry/types'
RSpec.describe LoanService::UseCases::Approval do
  let!(:pending_loan) { create(:loan, loan_status: Loan::statuses[:pending]) }
  let!(:paid_loan) { create(:loan, loan_status: Loan::statuses[:paid]) }
  let!(:approved_loan) { create(:loan, loan_status: Loan::statuses[:approved]) }
    
  describe '#run' do
    it 'approves the loan for pending loan uuid' do
      approval_use_case = described_class.new(loan_uuid: pending_loan.uuid)
      loan = Loan.find(pending_loan.id)
      expect(loan.status).to eq('pending') #initial status in DB
      
      result = approval_use_case.run
      expect(result.status).to eq(ResponseEntity::SUCCESS)
      loan = Loan.find(pending_loan.id)
      expect(loan.status).to eq('approved') #status updated in DB
    end

    it 'returns error response for paid loan' do
      approval_use_case = described_class.new(loan_uuid: paid_loan.uuid)
      result = approval_use_case.run
      expect(result.status).to eq(ResponseEntity::ERROR)
      loan = Loan.find(paid_loan.id)
      expect(loan.status).to eq('paid')
    end

    it 'returns error response for alread approved loan' do
      approval_use_case = described_class.new(loan_uuid: approved_loan.uuid)
      result = approval_use_case.run
      expect(result.status).to eq(ResponseEntity::ERROR)
      loan = Loan.find(approved_loan.id)
      expect(loan.status).to eq('approved')
    end
  end
end
