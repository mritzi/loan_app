require 'dry-struct'
require 'dry-types'
# Autoload 'app/models' directory
Rails.application.config.autoload_paths += %W(#{Rails.root}/app/models)

module Types
  include Dry.Types()
end
