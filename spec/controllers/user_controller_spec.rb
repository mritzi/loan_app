require 'rails_helper'
RSpec.describe UserController, type: :controller do
  let(:password) {'12345'}
  let(:active_user) { create(:user, password: password) }
  let(:inactive_user) { create(:user, password: password, active: false) }
  let(:valid_params) { { email: active_user.email, password: password} }
  let(:invalid_params) { { email: 'invalid@example.com', password: nil } }
  let(:inactive_user_params) { { email: inactive_user.email, password: password } }
  
  describe 'POST #login' do
    
    it 'returns a success response with valid credentials' do
      post :login, params: valid_params, format: :json
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)['auth_token']).to eq(active_user.auth_token)
    end
    
    it 'returns an error response with invalid credentials' do
      post :login, params: invalid_params, format: :json
      expect(response).to have_http_status(:unprocessable_entity)
    end
    
    it 'returns an error response with inactive user' do
      post :login, params: inactive_user_params, format: :json
      expect(JSON.parse(response.body)).to include('error' => 'Unprocessable request content')
    end
  end

  describe 'POST #register' do
    let(:new_user_params) {{ email: 'new_user@example.com', password: password}}
    it 'registers a new user with valid credentials' do
      post :register, params: new_user_params, format: :json
      expect(response).to have_http_status(:ok)
      new_user = User.find_by(email: new_user_params[:email])
      expect(JSON.parse(response.body)['auth_token']).to eq(new_user.auth_token)
    end

    it 'returns an error response with duplicate credentials' do
      post :register, params: new_user_params, format: :json
      expect(response).to have_http_status(:ok)
      #retry
      post :register, params: new_user_params, format: :json
      expect(response).to have_http_status(:not_acceptable)
    end
    
    it 'returns an error response with invalid credentials' do
      post :register, params: invalid_params, format: :json
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

end
