module LoanService
  module Entities
    class InstallmentPayment < Dry::Struct

      attribute :payment_ref_id, Types::Params::String
      attribute :amount, Types::Params::Decimal.constrained(gt: 0)
      attribute :loan, Types::Instance(::Loan)
      attribute :pending_installments, Types::Array.of(Types::Instance(::Installment))
      attribute :total_outstanding, Types::Params::Decimal.constrained(gt: 0)

    end
  end
end
