module LoanService
  module UseCases
    class Apply < Dry::Struct
      attribute :entity, LoanService::Entities::Apply

      PAYMENT_FREQ = 7

      def run
        uuid = Repositories::Apply.persist(
          entity: entity
        )
        installment_entities = generate_installments(
          Repositories::LoanInfo.find_by(uuid: uuid)
        )
        Repositories::Installment.create(installment_entities: installment_entities)
        response_entity(uuid)
      end

      private

      def response_entity(application_uuid)
        ResponseEntity.new(
          status: ResponseEntity::SUCCESS,
          data: {message: "Loan application number is: #{application_uuid}"}
        )
      end

      private

      def generate_installments(loan)
        installments = []
        installment_date = loan.start_date
        expected_amount = (loan.amount.to_f / loan.term).round(2)
        round_off = (loan.amount.to_f - expected_amount*loan.term).round(2)

        loan.term.times do 
          installment_date += PAYMENT_FREQ #weekly schedule
          amount = loan.recovery_date != installment_date ? 
            expected_amount : (expected_amount+round_off) 
          
          installments << Entities::Installment.new(
            loan_id: loan.id,
            uuid: UniqueIdGenerator.uuid,
            expected_amount: amount,
            expected_pay_date: installment_date
          )
        end
        installments
      end


    end
  end
end
