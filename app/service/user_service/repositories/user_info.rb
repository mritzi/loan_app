module UserService
  module Repositories
    class UserInfo

      def self.find_by(email: )
        return nil if email.blank?
        User.find_by(email: email)
      end

    end
  end
end
