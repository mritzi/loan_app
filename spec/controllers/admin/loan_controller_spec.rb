require 'rails_helper'
require 'dry/types'
RSpec.describe Admin::LoanController, type: :controller do
  let!(:active_user) { create(:user) }
  let!(:administrator) { create(:administrator) }
  let!(:admin_headers) { { 'x-user-email' => administrator.user.email, 'auth-token' => administrator.user.auth_token } }
  let!(:invalid_headers) { { 'x-user-email' => 'invalid_user@example.com', 'auth-token' => 'lorem_ipsum' } }
  let!(:new_user) { create(:user) }
  let!(:new_user_headers) { { 'x-user-email' => new_user.email, 'auth-token' => new_user.auth_token } }
  let!(:pending_loan) do
    apply_attributes = { amount: 5000, term: 2 }
    entity = LoanService::Entities::Helpers::Apply.get_entity(
      apply_attributes, active_user.id
    )
    LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    Loan.last
  end
  let!(:approved_loan) {create(:loan, loan_status: Loan.statuses[:approved])}
  let!(:paid_loan) {create(:loan, loan_status: Loan.statuses[:paid])}


  describe 'POST #approve' do
    let(:valid_attributes) { { loan_uuid: pending_loan.uuid } }
    let(:invalid_attributes) { { loan_uuid: 'lorem_ipsum'} }
  
    it 'approves a loan with admin attributes & headers' do
      request.headers.merge!(admin_headers)
      post :approve, params: valid_attributes, format: :json
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)["message"].include?("loan approval")).to eq(true)
      loan = Loan.where(uuid: pending_loan.uuid).last
      expect(loan.status).to eq("approved")
    end

    it 'returns an error response with valid attributes but invalid headers' do
      request.headers.merge!(invalid_headers)
      post :approve, params: valid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response with invalid attributes & invalid headers' do
      request.headers.merge!(invalid_headers)
      post :approve, params: invalid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response when non admin tries to approve loan' do
      request.headers.merge!(new_user_headers)
      post :approve, params: valid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response for invalid params' do
      request.headers.merge!(admin_headers)
      post :approve, params: invalid_attributes, format: :json
      expect(response).to have_http_status(:not_acceptable)
    end

    it 'returns an error response for approved loan' do
      request.headers.merge!(admin_headers)
      post :approve, params: {loan_uuid: approved_loan.uuid}, format: :json
      expect(response).to have_http_status(:not_acceptable)
    end

    it 'returns an error response for paid loan' do
      request.headers.merge!(admin_headers)
      post :approve, params: {loan_uuid: paid_loan.uuid}, format: :json
      expect(response).to have_http_status(:not_acceptable)
    end

  end

end
