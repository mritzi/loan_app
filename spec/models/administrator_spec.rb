require "rails_helper"
RSpec.describe Administrator, :type => :model do
  before(:all) do
    @admin1 = create(:administrator)
  end
  
  it "is valid with valid attributes" do
    expect(@admin1).to be_valid
    db_admin = Administrator.find(@admin1.id)
    db_user = User.find(db_admin.user_id)
    expect(db_admin.created_by).not_to be_nil
    expect(db_user).not_to be_nil
  end
  
  it "can be marked inactive" do
    db_admin = Administrator.find(@admin1.id)
    expect(db_admin.is_active).to eq(true)
    db_admin.is_active = false
    db_admin.save!
    db_admin = Administrator.find(@admin1.id)
    expect(db_admin.is_active).to eq(false)
  end
  
end
