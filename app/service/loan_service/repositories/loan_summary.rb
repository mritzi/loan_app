module LoanService
  module Repositories
    class LoanSummary
      
      def self.get_entity(loan_uuid: )
        loan = Loan.includes(:installments).find_by(uuid: loan_uuid) 
        return prepare_entity(loan, loan.installments)
      end

      private
      
      def self.prepare_entity(loan, installments)
        loan_entity = ViewLoans.get_entities(
          user_id: loan.user_id
        ).first
        Entities::LoanSummary.new(
          loan: loan_entity,
          installments: get_installment_entities(installments)
        )
      end

      def self.get_installment_entities(installments)
        installments.map do |installment|
          Entities::ViewInstallment.new(
            ref_id: installment.uuid,
            expected_amount: installment.expected_amount,
            expected_pay_date: installment.expected_pay_date,
            status: installment.status,
            actual_amount: installment.actual_amount,
            actual_pay_date: installment.actual_pay_date,
            
          )
        end
      end

    end
  end
end
