class User < ApplicationRecord
  before_create :generate_uuid

  def self.encrypt_password(input_password)
    BCrypt::Password.create(input_password)
  end

  def set_auth_params
    self.auth_token = SecureRandom.hex
    self.token_expiry = Time.now + 1.day  # Example: Set expiry to 1 day from now
  end

  def is_valid_password(input_password)
    BCrypt::Password.new(self.password_hash) == input_password
  end

  private

  def generate_uuid
    self.uuid = SecureRandom.uuid unless uuid.present?
  end
  
end