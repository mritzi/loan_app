module LoanService
  module Entities
    class Installment < Dry::Struct

      attribute :loan_id, Types::Strict::Integer
      attribute :uuid, Types::Strict::String
      attribute :expected_amount, Types::Params::Decimal
      attribute :expected_pay_date, Types::Params::Date
      attribute :status, Types::Strict::Integer.default(
        ::Installment.statuses[:pending]
      )
      
    end
  end
end
