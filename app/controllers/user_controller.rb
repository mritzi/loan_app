class UserController < ApplicationController
  respond_to :json
  before_action :authenticate_user, only: [:logout, :change_password]
  
  def login
    return if !valid_params(params, [:email, :password])
    user = User.find_by(email: params[:email].to_s, is_active: true)
    if user.present? && user.is_valid_password(params[:password].to_s)
      return render_success_response({auth_token: user.auth_token})
    end
    return render_422_error
  end

  def register
    return if !valid_params(params, [:email, :password])
    begin 
      entity = UserService::Entities::Access.new(
        email: params[:email],
        password_hash: User.encrypt_password(params[:password].to_s)
      )
    rescue Dry::Types::ConstraintError => ex
      puts ex.message
      return render_422_error
    end

    response_entity = UserService::UseCases::Register.new(
      entity: entity
    ).run
    render_response_entity(response_entity)
  end

  def recover
    #to be implemented later
  end

  def logout
    # to be implemented later
  end

  def change_password
    #to be implemented later
  end

end