require 'rails_helper'
require 'dry/types'
RSpec.describe LoanService::Repositories::Installment do
  let!(:active_user) { create(:user) }
  let(:apply_attributes) { { amount: 5000, term: 2 } }
  let!(:approved_loan) do
    apply_attributes = { amount: 5000, term: 2 }
    entity = LoanService::Entities::Helpers::Apply.get_entity(
      apply_attributes, active_user.id
    )
    LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    loan = Loan.last
    loan.status = Loan.statuses[:approved]
    loan.save!
    Loan.find(loan.id)
  end
  let(:valid_attributes) { { amount: (approved_loan.amount * 0.8), loan_uuid: approved_loan.uuid } }
  let(:payment_ref_id) { UniqueIdGenerator.hex }
  
  describe '#create' do
    let!(:pending_loan) { create(:loan, loan_status: Loan.statuses[:pending])}
    it 'records installment records for pending loan with valid attributes' do
      installmemt_entity =  LoanService::Entities::Installment.new(
        loan_id: pending_loan.id,
        uuid: UniqueIdGenerator.uuid,
        expected_amount: pending_loan.amount,
        expected_pay_date: pending_loan.recovery_date
      )
      expect(pending_loan.term).to eq(1)
      pending_installments = pending_loan.installments.pending
      expect(pending_installments.length).to eq(0)
      
      described_class.create(installment_entities: [installmemt_entity])
      pending_installments = pending_loan.installments.pending
      expect(pending_installments.length).to eq(1)
      
    end
  end

  describe '#payment_update' do
    it 'records successfull payment for approved loan with valid attributes' do
      payment_ref_id = UniqueIdGenerator.hex
      entity = LoanService::Entities::Helpers::InstallmentPayment.get_entity(
        valid_attributes, payment_ref_id
      )
      
      paid_installments = Loan.find(approved_loan.id).installments.paid
      expect(paid_installments.length).to eq(0) # no installmemts paid

      is_success = described_class.payment_update(payment_entity: entity)
      expect(is_success).to eq(true)
      paid_installments = Loan.find(approved_loan.id).installments.paid
      expect(paid_installments.length).to eq(1) # first installmemt marked paid in DB
      expect(paid_installments.first.actual_amount).to eq(entity.amount)
    end

  end

  describe '#recompute_pending_payments' do
  it 'updates expected amount for other pending installments if one installment amount is higher' do
    payment_ref_id = UniqueIdGenerator.hex
    entity = LoanService::Entities::Helpers::InstallmentPayment.get_entity(
      valid_attributes, payment_ref_id
    )
    is_success = described_class.payment_update(payment_entity: entity)
    expect(is_success).to eq(true)
    paid_installments = Loan.find(approved_loan.id).installments.paid
    expect(paid_installments.length).to eq(1)

    #test case for 2nd installment data
    pending_installment = Loan.find(approved_loan.id).installments.pending.first
    intial_expected_amount = pending_installment.expected_amount
    described_class.recompute_pending_payments(payment_entity: entity)
    db_installment_rec = Installment.find(pending_installment.id)
    expect(db_installment_rec.expected_amount).not_to eq(intial_expected_amount)
    expect(db_installment_rec.expected_amount).to be < intial_expected_amount
  end

  end

end
