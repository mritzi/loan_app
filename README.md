# README

# Loan App - Ruby on Rails Application

This application is a loan management system built using Ruby on Rails. It provides functionalities for users to sign up, log in, apply for a loan, manage loan approvals, pay installments, and check the status of their loans.

## Features

- **User Authentication:**
  - Users can sign up with their details (name, email, password) and log in securely.

- **Loan Application:**
  - Authenticated users can apply for a loan by providing necessary details.
  
- **Loan Approval:**
  - Admins or authorized personnel can review loan applications and approve them.
  
- **Installment Payments:**
  - Users can pay their loan installments securely within the application.

- **Loan Details:**
  - Users can check the details of their loans, including installment details.

- **Loans Info:**
  - Users can get list of all loans they have applied for with basic details.

## Getting Started

### Prerequisites

- Ruby (version 2.7.2p137)
- Rails (version 6.0.6.1)
- PostgreSQL database (version 14.10)

## Assumptions
  - Google doc provided via email

### Installation

  - Skipped, and provided Heroku API & cUrl snippets to invoke APIs in the google drive folder
  - Please reach out to me with preferred OS name (preferrably: MacOS/Linux), if the team prefers local installation, and I'll share relevant details.

### Test Results

  ![plot](/app/assets/images/loan_app_test_results.png)


### Notes about relevant files
  - API Controllers in "controllers" folder
  - API routes defined in "routes.rb" file
  - Business logic in "service" folder
  - Test cases in "spec" folder
  - ORM classes in "models" folder
