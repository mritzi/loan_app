module LoanService
  module UseCases
    class InstallmentPayment < Dry::Struct
      attribute :entity, Entities::InstallmentPayment

      def run
        response_entity = run_validations
        return response_entity if response_entity.status == ResponseEntity::ERROR

        ::Installment.transaction do #supports rollback
          Repositories::Installment.payment_update(payment_entity: entity)
          Repositories::Installment.recompute_pending_payments(payment_entity: entity)
          Repositories::LoanInfo.recompute_status(payment_entity: entity)
        end
        response_entity(entity.payment_ref_id)
      end

      private

      def run_validations
        return error_response('Invalid amount') if invalid_amount
        return error_response('Loan invalid for payment') if invalid_loan
        return ResponseEntity.new(status: ResponseEntity::SUCCESS, data: {})
      end

      def invalid_amount
        entity.amount > entity.total_outstanding || 
        entity.amount < entity.pending_installments.first.expected_amount
      end

      def invalid_loan
        !entity.loan.approved?
      end

      def error_response(message)
        ResponseEntity.new(
          status: ResponseEntity::ERROR,
          data: {message: message}
        )
      end

      def response_entity(payment_ref_id)
        ResponseEntity.new(
          status: ResponseEntity::SUCCESS,
          data: {message: "Installment payment received with ref id: #{payment_ref_id}"}
        )
      end

    end
  end
end
