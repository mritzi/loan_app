require 'rails_helper'
require 'dry/types'
RSpec.describe LoanService::UseCases::LoanSummary do
  let!(:active_user) { create(:user) }
  let!(:approved_loan) do
    apply_attributes = { amount: 5000, term: 2 }
    entity = LoanService::Entities::Helpers::Apply.get_entity(
      apply_attributes, active_user.id
    )
    LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    loan = Loan.last
    loan.status = Loan.statuses[:approved]
    loan.save!
    Loan.find(loan.id)
  end

  describe '#run' do

    it 'returns details only for provided loan uuid' do
      response_entity = described_class.new(loan_uuid: approved_loan.uuid).run
      expect(response_entity.status).to eq(ResponseEntity::SUCCESS)
      expect(response_entity.data["loan"]["ref_id"]).to eq(approved_loan.uuid)
      expect(response_entity.data["installments"].length).to eq(approved_loan.term)
    end
  end

end