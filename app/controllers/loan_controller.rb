class LoanController < ApplicationController
  respond_to :json
  before_action :authenticate_user
  
  def apply
    return if !valid_params(params, [:amount, :term])
    begin
      user_id = get_user_id(request.headers['x-user-email'])
      entity = LoanService::Entities::Helpers::Apply.get_entity(
        params, user_id
      )
    rescue Dry::Types::ConstraintError, Dry::Struct::Error => ex
      puts ex.message #log line for demo purpose only
      #Sentry/Datadog/etc... should ideally be used for error logging
      return render_422_error
    end
    response_entity = LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    return render_response_entity(response_entity)
  end

  def pay_installment
    return if !valid_params(params, [:amount, :loan_uuid])
    user_id = get_user_id(request.headers['x-user-email'])
    loan_uuid = params[:loan_uuid]
    return render_401_error if !is_valid_user_loan(
      request.headers['x-user-email'], params[:loan_uuid]
    )

    begin
      payment_ref_id = UniqueIdGenerator.hex
      #payment_ref_id = PaymentService::Collect.new(details: details).run
      entity = LoanService::Entities::Helpers::InstallmentPayment.get_entity(
        params, payment_ref_id
      )
    rescue Dry::Types::ConstraintError, Dry::Struct::Error => ex
      return render_422_error
    end

    #assumed that payment will be successfully collected via payment gateway
    response_entity = LoanService::UseCases::InstallmentPayment.new(
      entity: entity
    ).run
    return render_response_entity(response_entity)
  end

  def list
    user_id = get_user_id(request.headers['x-user-email'])
    response_entity = LoanService::UseCases::ViewLoans.new(
      user_id: user_id
    ).run
    return render_response_entity(response_entity)
  end

  def detail
    return if !valid_params(params, [:loan_uuid])
    return render_401_error if !is_valid_user_loan(
      request.headers['x-user-email'], params[:loan_uuid]
    )
    
    response_entity = LoanService::UseCases::LoanSummary.new(
      loan_uuid: params[:loan_uuid]
    ).run
    return render_response_entity(response_entity)
  end

  private 

  def get_user_id(email)
    user_id = UserService::Repositories::UserInfo.find_by(
      email: email
    ).try(:id)
  end

  def is_valid_user_loan(email, loan_uuid)
    loan = LoanService::Repositories::LoanInfo.find_by(
      uuid: loan_uuid
    )
    loan.present? && loan.approved? && (get_user_id(email) == loan.try(:user_id))
  end

end