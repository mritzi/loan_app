module LoanService
  module UseCases
    class ViewLoans < Dry::Struct
      attribute :user_id, Types::Strict::Integer

      def run
        loan_entities = Repositories::ViewLoans.get_entities(
          user_id: user_id
        )
        response_entity(loan_entities)
      end

      private
      
      def response_entity(loan_entities)
        ResponseEntity.new(
          status: ResponseEntity::SUCCESS,
          data: {loans: JSON.parse(loan_entities.to_json)}
        )
      end

    end
  end
end
