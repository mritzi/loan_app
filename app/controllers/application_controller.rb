class ApplicationController < ActionController::API
  include ActionController::MimeResponds

  INVALID_PARAMS_MSG = 'Unprocessable request content'
  AUTH_FAILURE_MSG = 'Unauthorised request'
  REQ_SUCCESS_GENERIC_MSG = 'Request submitted successfully'

  def authenticate_user
    auth_token = request.headers['auth-token']
    email = request.headers['x-user-email'].to_s
    is_valid = UserService::Repositories::Authenticate.new(
      email: email,
      auth_token: auth_token
    ).is_valid_user
    render_401_error if !is_valid
  end

  def authenticate_admin
    auth_token = request.headers['auth-token']
    email = request.headers['x-user-email'].to_s
    is_valid = UserService::Repositories::Authenticate.new(
      email: email,
      auth_token: auth_token
    ).is_valid_admin
    render_401_error if !is_valid
  end

  def valid_params(params, keys)
    keys.each do |key|
      if params[key].blank?
        puts "#{key} val not found in params"
        render_422_error
        return false
      end
    end
    return true
  end

  def render_response_entity(response_entity)
    response_entity.status == ResponseEntity::SUCCESS ?
      render_success_response(response_entity.data) : 
      render_4xx_response(406, response_entity.data)
  end

  def render_success_response(response_obj)
    respond_to do |format|
      format.json { json_response(200, response_obj) }
    end
  end

  def render_success_message(message)
    render_success_response({message: message})
  end

  def render_401_error
    render_4xx_error(401, AUTH_FAILURE_MSG)
  end

  def render_422_error
    render_4xx_error(422, INVALID_PARAMS_MSG)
  end

  def render_4xx_error(code, message)
    render_4xx_response(code, {error: message})
  end

  def render_4xx_response(code, response_obj)
    fail "invalid error code" if code.to_i < 400 || code.to_i > 499
    respond_to do |format|
      format.json { json_response(code, response_obj) }
    end
  end

  private

  def json_response(code, message_obj)
    render json:message_obj.to_json, status: code
  end

end
