module LoanService
  module Entities
    class ViewInstallment < Dry::Struct

      attribute :ref_id, Types::Strict::String
      attribute :expected_amount, Types::Params::Decimal
      attribute :expected_pay_date, Types::Params::Date
      attribute :status, Types::Strict::String
      attribute :actual_amount, Types::Params::Decimal.optional
      attribute :actual_pay_date, Types::Params::Date.optional
      
    end
  end
end
