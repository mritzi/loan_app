require 'rails_helper'
require 'dry/types'
RSpec.describe LoanService::Repositories::ViewLoans do
  let!(:active_user) { create(:user) }
  let!(:approved_loan) do
    apply_attributes = { amount: 5000, term: 2 }
    entity = LoanService::Entities::Helpers::Apply.get_entity(
      apply_attributes, active_user.id
    )
    LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    loan = Loan.last
    loan.status = Loan.statuses[:approved]
    loan.save!
    Loan.find(loan.id)
  end
  let!(:pending_loan) { create(:loan, loan_status: Loan.statuses[:pending])} # created for diff user
  
  describe '#get_entities' do

    it 'returns info of all loans only for relevant user' do
      loan_entities = described_class.get_entities(user_id: active_user.id)
      expect(loan_entities.length).to eq(1)
      expect(loan_entities.first.ref_id).to eq(approved_loan.uuid)
      expect(loan_entities.first.ref_id).not_to eq(pending_loan.uuid)
    end
  end

end