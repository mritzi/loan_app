module UserService
  module UseCases
    class Register < Dry::Struct
      attribute :entity, Entities::Access

      def run
        return existing_user_response if existing_user.present?
        auth_token = Repositories::Register.persist(
          entity: entity
        )
        response_entity(auth_token)
      end

      private

      
      def existing_user_response
        ResponseEntity.new(
          status: ResponseEntity::ERROR,
          data: {message: 'User already exists'}
        )
      end

      def response_entity(auth_token)
        ResponseEntity.new(
          status: ResponseEntity::SUCCESS,
          data: {auth_token: auth_token}
        )
      end

      def existing_user
        Repositories::Register.find_user(email: entity.email)
      end

    end
  end
end
