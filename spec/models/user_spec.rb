require "rails_helper"
RSpec.describe User, :type => :model do
  
  let!(:user1) {create(:user)}
  
  it "is valid with valid attributes" do
    expect(user1).to be_valid
    db_user = User.find(user1.id)
    expect(db_user.uuid).not_to be_nil
    expect(db_user.token_expiry).not_to be_nil
  end
  
  it "is not valid with duplicate email" do
    expect {
      user2 = create(:user, email: user1.email)
    }.to raise_error(ActiveRecord::RecordNotUnique) { |error|
      expect(error.message).to include("(#{user1.email}) already exists")
    }
  end
  
  it "is not valid without an email" do
    expect {
      user2 = create(:user, email: nil)
    }.to raise_error(ActiveRecord::NotNullViolation) { |error|
      expect(error.message).to include("email")
      expect(error.message).to include("violates not-null constraint")
    }
  end

  it "is not valid without passsword hash" do
    expect {
      user2 = create(:user, password_hash: nil)
    }.to raise_error(ActiveRecord::NotNullViolation) { |error|
      expect(error.message).to include("password_hash")
      expect(error.message).to include("violates not-null constraint")
    }
  end

  it "is not valid without auth token" do
    expect {
      user2 = create(:user, auth_token: nil)
    }.to raise_error(ActiveRecord::NotNullViolation) { |error|
      expect(error.message).to include("auth_token")
      expect(error.message).to include("violates not-null constraint")
    }
  end

end