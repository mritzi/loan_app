require 'rails_helper'
require 'dry/types'
RSpec.describe LoanController, type: :controller do
  let!(:active_user) { create(:user) }
  let!(:inactive_user) { create(:user, active: false) }
  let!(:valid_headers) { { 'x-user-email' => active_user.email, 'auth-token' => active_user.auth_token } }
  let!(:invalid_headers) { { 'x-user-email' => 'invalid_user@example.com', 'auth-token' => 'lorem_ipsum' } }
  let!(:inactive_user_headers) { { 'x-user-email' => inactive_user.email, 'auth-token' => inactive_user.auth_token } }
  let!(:approved_loan_data) do
    apply_attributes = { amount: 5000, term: 2 }
    entity = LoanService::Entities::Helpers::Apply.get_entity(
      apply_attributes, active_user.id
    )
    LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    loan = Loan.last
    loan.status = Loan.statuses[:approved]
    loan.save!
    user = User.find(loan.user_id)
    {
      loan: Loan.find(loan.id),
      user: user,
      headers: { 'x-user-email' => user.email, 'auth-token' => user.auth_token }
    }
  end

  describe 'POST #apply' do
    let(:valid_attributes) { { amount: 5000, term: 2 } }
    let(:invalid_attributes) { { amount: -100, term: 6 } }
  
    it 'applies for a loan with valid attributes & headers' do
      request.headers.merge!(valid_headers)
      post :apply, params: valid_attributes, format: :json
      expect(response).to have_http_status(:ok)
      loan = Loan.where(user_id: active_user.id).last
      expect(JSON.parse(response.body)["message"].include?(loan.uuid)).to eq(true)
      expect(loan.installments.length).to eq(loan.term)
    end

    it 'returns an error response with valid attributes but invalid headers' do
      request.headers.merge!(invalid_headers)
      post :apply, params: valid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response with invalid attributes & invalid headers' do
      request.headers.merge!(invalid_headers)
      post :apply, params: invalid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response for inactive users' do
      request.headers.merge!(inactive_user_headers)
      post :apply, params: valid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response for invalid params' do
      request.headers.merge!(valid_headers)
      post :apply, params: invalid_attributes, format: :json
      expect(response).to have_http_status(:unprocessable_entity)
    end

  end

  describe 'POST #pay_installment' do
    let!(:pending_loan) { create(:loan, loan_status: Loan::statuses[:pending]) }
    let!(:paid_loan) { create(:loan, loan_status: Loan::statuses[:paid]) }
    let(:valid_attributes) { { amount: (approved_loan_data[:loan].amount * 0.8), loan_uuid: approved_loan_data[:loan].uuid } }
    let(:invalid_attributes) { { amount: -100, loan_uuid: 'lorem_ipsum' } }

    it 'pays loan installment with valid attributes & headers' do
      request.headers.merge!(approved_loan_data[:headers])
      post :pay_installment, params: valid_attributes, format: :json
      expect(response).to have_http_status(:ok)
      loan = Loan.where(user_id: active_user.id).last
      payment_detail = loan.installments.first.payment
      expect(JSON.parse(response.body)["message"].include?(payment_detail.payment_ref_id)).to eq(true)
      expect(loan.approved?).to eq(true)
      expect(loan.paid?).to eq(false)
    end

    it 'returns an error response with valid attributes but invalid headers' do
      request.headers.merge!(invalid_headers)
      post :pay_installment, params: valid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response with invalid attributes & invalid headers' do
      request.headers.merge!(invalid_headers)
      post :pay_installment, params: invalid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response for inactive users' do
      request.headers.merge!(inactive_user_headers)
      post :pay_installment, params: valid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns an error response for invalid params' do
      request.headers.merge!(valid_headers)
      post :pay_installment, params: invalid_attributes, format: :json
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'GET #list' do
    let!(:new_loan) { create(:loan) } # created for diff user
    let!(:new_user) { create(:user) } # this is yet another diff user
    let(:new_user_headers) { { 'x-user-email' => new_user.email, 'auth-token' => new_user.auth_token } }
    
    it 'show loans for active user & valid headers' do
      request.headers.merge!(approved_loan_data[:headers])
      get :list, format: :json
      expect(response).to have_http_status(:ok)
      loans = Loan.where(user_id: active_user.id)
      expect(JSON.parse(response.body)["loans"][0]["ref_id"]).to eq(loans.first.uuid)
      expect(JSON.parse(response.body)["loans"].length).to eq(loans.length)
    end

    it 'show loans for active user & invalid headers' do
      request.headers.merge!(invalid_headers)
      get :list, format: :json
      expect(response).to have_http_status(:unauthorized)
      expect(JSON.parse(response.body)["loans"]).to eq(nil)
    end

    it 'doesnt include loan info of other users' do
      request.headers.merge!(approved_loan_data[:headers])
      get :list, format: :json
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)["loans"].to_s.include?(new_loan.uuid)).to eq(false)
    end

    it 'returns empty response for inactive users with no loan' do
      request.headers.merge!(new_user_headers)
      get :list, format: :json
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)["loans"].length).to eq(0)
    end
  end

  describe 'GET #detail' do
    let!(:new_loan) { create(:loan) } # created for diff user
    let!(:new_user) { create(:user) } # this is yet another diff user
    let(:new_user_headers) { { 'x-user-email' => new_user.email, 'auth-token' => new_user.auth_token } }
    let(:api_params) { {loan_uuid: approved_loan_data[:loan].uuid} }

    it 'shows loan details for active user & valid headers' do
      request.headers.merge!(approved_loan_data[:headers])
      get :detail, params: api_params, format: :json
      expect(response).to have_http_status(:ok)
      loan = Loan.where(user_id: active_user.id).first
      expect(JSON.parse(response.body)["loan"]["ref_id"]).to eq(loan.uuid)
      expect(JSON.parse(response.body)["installments"].length).to eq(loan.term)
    end

    it 'returns errors for valid loan but invalid headers' do
      request.headers.merge!(invalid_headers)
      get :detail, params: api_params, format: :json
      expect(response).to have_http_status(:unauthorized)
      expect(JSON.parse(response.body)["loan"]).to eq(nil)
      expect(JSON.parse(response.body)["installment"]).to eq(nil)
    end

    it 'returns error response if loan uuid is missing' do
      request.headers.merge!(approved_loan_data[:headers])
      get :detail, params: {loan_uuid: nil}, format: :json
      expect(response).to have_http_status(:unprocessable_entity)
      expect(JSON.parse(response.body)["loan"]).to eq(nil)
    end

    it 'returns empty response for invalid loan uuid' do
      request.headers.merge!(approved_loan_data[:headers])
      get :detail, params: {loan_uuid: 'lorem_ipsum'}, format: :json
      expect(response).to have_http_status(:unauthorized)
      expect(JSON.parse(response.body)["loan"]).to eq(nil)
    end
  end

end
