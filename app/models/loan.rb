class Loan < ApplicationRecord
  belongs_to :user, class_name: User.name, primary_key: :id, foreign_key: :user_id
  has_many :installments, class_name: Installment.name, primary_key: :id, foreign_key: :loan_id

  enum status: {
    pending: 0,
    approved: 1,
    rejected: 2,
    paid: 3
    # can add more statuses
  }

  def pending_installments
    self.installments.select{|inst| 
      inst.pending?
    }.sort_by{|inst|
      inst.expected_pay_date
    }
  end
end