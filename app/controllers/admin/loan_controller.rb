class Admin::LoanController < ApplicationController
  before_action :authenticate_admin

  def approve
    return if !valid_params(params, [:loan_uuid])
    loan_uuid = params[:loan_uuid].to_s
    response_entity = LoanService::UseCases::Approval.new(loan_uuid: loan_uuid).run
    return render_response_entity(response_entity)
  end

  def list
    # to be implemented later
  end

  def reject
    # to be implemented later
  end

end