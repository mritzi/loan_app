class Administrator < ApplicationRecord
  belongs_to :user, class_name: User.name, primary_key: :id, foreign_key: :user_id
  belongs_to :creator, class_name: User.name, primary_key: :id, foreign_key: :created_by
  enum role: { moderator: 0, admin: 1}
end