# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_12_12_153549) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "administrators", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "role", null: false
    t.integer "created_by", null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_administrators_on_user_id", unique: true
  end

  create_table "installments", force: :cascade do |t|
    t.string "uuid", null: false
    t.integer "loan_id", null: false
    t.float "expected_amount", null: false
    t.float "actual_amount"
    t.date "expected_pay_date", null: false
    t.date "actual_pay_date"
    t.integer "status", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["loan_id"], name: "index_installments_on_loan_id"
    t.index ["uuid"], name: "index_installments_on_uuid", unique: true
  end

  create_table "loans", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "uuid", null: false
    t.float "amount", null: false
    t.integer "term", null: false
    t.integer "status", null: false
    t.date "start_date", null: false
    t.date "recovery_date", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_loans_on_user_id"
    t.index ["uuid"], name: "index_loans_on_uuid", unique: true
  end

  create_table "payment_details", force: :cascade do |t|
    t.integer "installment_id", null: false
    t.string "payment_ref_id", null: false
    t.string "payment_gateway", null: false
    t.integer "status", null: false
    t.float "amount", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["installment_id"], name: "index_payment_details_on_installment_id"
    t.index ["payment_ref_id"], name: "index_payment_details_on_payment_ref_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", null: false
    t.string "uuid", null: false
    t.string "password_hash", null: false
    t.string "auth_token", null: false
    t.datetime "token_expiry", null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["auth_token"], name: "index_users_on_auth_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["uuid"], name: "index_users_on_uuid", unique: true
  end

end
