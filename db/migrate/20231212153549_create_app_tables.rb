class CreateAppTables < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email, null: false
      t.string :uuid, null: false
      t.string :password_hash, null: false
      t.string :auth_token, null: false
      t.datetime :token_expiry, null: false
      t.boolean :is_active, default: true, null: false
      
      t.timestamps
    end

    create_table :administrators do |t|
      t.integer :user_id, null: false
      t.integer :role, null: false
      t.integer :created_by, null: false
      t.boolean :is_active, default: true, null: false

      t.timestamps
    end

    create_table :loans do |t|
      t.integer :user_id, null: false
      t.string :uuid, null: false
      t.float :amount, null: false
      t.integer :term, null: false
      t.integer :status, null: false
      t.date :start_date, null: false
      t.date :recovery_date, null: false

      t.timestamps
    end

    create_table :installments do |t|
      t.string :uuid, null: false
      t.integer :loan_id, null: false
      t.float :expected_amount, null: false
      t.float :actual_amount
      t.date :expected_pay_date, null: false
      t.date :actual_pay_date
      t.integer :status, null: false

      t.timestamps
    end
    
    create_table :payment_details do |t|
      t.integer :installment_id, null: false
      t.string :payment_ref_id, null: false
      t.string :payment_gateway, null: false
      t.integer :status, null: false
      t.float :amount, null: false

      t.timestamps
    end
    
    add_index :users, :email, unique: true
    add_index :users, :uuid, unique: true
    add_index :users, :auth_token, unique: true

    add_index :administrators, :user_id, unique: true

    add_index :loans, :user_id
    add_index :loans, :uuid, unique: true

    add_index :installments, :uuid, unique: true
    add_index :installments, :loan_id

    add_index :payment_details, :payment_ref_id, unique: true
    add_index :payment_details, :installment_id

  end
end
