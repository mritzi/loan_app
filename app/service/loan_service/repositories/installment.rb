module LoanService
  module Repositories
    class Installment
      
      def self.create(installment_entities: )
        #simple implementation, robust solution will be to do bulk upsert with additional code
        ::Installment.transaction do #supports rollback
          installment_entities.each do |entity|
            rec = map_entity_to_new_rec(entity)
            rec.save!
          end
        end
      end

      def self.payment_update(payment_entity: )
        installment = payment_entity.pending_installments.first
        installment.actual_amount = payment_entity.amount
        installment.status = ::Installment.statuses[:paid]
        installment.actual_pay_date = Date.today

        installment.save!
        save_payment_info(payment_entity, installment.id)
      end
      
      #if amount higher than one installment is paid, then other pending installment amounts are re-computed
      def self.recompute_pending_payments(payment_entity: )
        first_installment = payment_entity.pending_installments.first
        pending_amount = (payment_entity.total_outstanding - payment_entity.amount).round(2)
        return nil if first_installment.expected_amount == payment_entity.amount

        other_installments = payment_entity.pending_installments.reject{|inst| 
          inst.id == first_installment.id
        }
        last_installment = other_installments.last
        
        expected_emi = (pending_amount / other_installments.length).round(2)
        round_off = (pending_amount - other_installments.length*expected_emi).round(2)

        other_installments.each do |inst|
          inst.expected_amount = expected_emi
          inst.expected_amount += round_off if inst.id == last_installment.id

          inst.status = ::Installment.statuses[:paid] if inst.expected_amount == 0.0
          inst.save!
        end

      end

      private

      def self.save_payment_info(payment_entity, installment_id)
        payment = PaymentDetail.new(
          installment_id: installment_id,
          payment_ref_id: payment_entity.payment_ref_id,
          payment_gateway: 'Billdesk', #fixed for now
          status: PaymentDetail.statuses[:successfull],
          amount: payment_entity.amount
        )
        payment.save!
      end

      def self.map_entity_to_new_rec(entity)
        ::Installment.new(
          loan_id: entity.loan_id,
          uuid: entity.uuid,
          expected_amount: entity.expected_amount,
          expected_pay_date: entity.expected_pay_date,
          status: entity.status
        )
      end

    end
  end
end