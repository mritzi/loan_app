module UserService
  module Repositories
    class Authenticate < Dry::Struct
      attribute :email, Types::Strict::String
      attribute :auth_token, Types::Strict::String

      def is_valid_user
        get_user.present?
      end

      def is_valid_admin
        is_valid_user && get_admin.present?
      end

      private 

      def get_user
        @user || User.find_by(
          email: email, auth_token: auth_token, is_active: true
        )
      end

      def get_admin
        @admin || Administrator.find_by(
          user_id: get_user.id, is_active: true
        )
      end


    end
  end
end
