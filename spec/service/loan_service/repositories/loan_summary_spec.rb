require 'rails_helper'
require 'dry/types'
RSpec.describe LoanService::Repositories::LoanSummary do
  let!(:active_user) { create(:user) }
  let!(:approved_loan) do
    apply_attributes = { amount: 5000, term: 2 }
    entity = LoanService::Entities::Helpers::Apply.get_entity(
      apply_attributes, active_user.id
    )
    LoanService::UseCases::Apply.new(
      entity: entity
    ).run
    loan = Loan.last
    loan.status = Loan.statuses[:approved]
    loan.save!
    Loan.find(loan.id)
  end

  describe '#get_entity' do

    it 'returns details only for provided loan uuid' do
      loan_entity = described_class.get_entity(loan_uuid: approved_loan.uuid)
      expect(loan_entity.loan.ref_id).to eq(approved_loan.uuid)
      expect(loan_entity.installments.length).to eq(approved_loan.term)
    end
  end

end