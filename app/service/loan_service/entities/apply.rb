module LoanService
  module Entities
    class Apply < Dry::Struct

      attribute :application_date, Types::Params::Date
      attribute :user_id, Types::Strict::Integer
      attribute :amount, Types::Params::Decimal.constrained(gt: 0)
      attribute :term, Types::Params::Integer.constrained(gt: 0)
      attribute :status, Types::Strict::Integer.default(
        Loan.statuses[:pending]
      )

    end
  end
end
